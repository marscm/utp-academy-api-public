# utp-academy-api-public

Comando necesario para poder desplegar un Microservicio en Cloud Run

### ENVIRONMENT VARIABLES

```
PROJECT_ID='<project_id>'
CLOUD_RUN_PROJECT_NAME='utp-lab-api'
DIRECTORY_PROJECT='./'
REGION='us-central1'
```

### DOCKERIZACION
```
gcloud builds submit --suppress-logs \
    --tag gcr.io/$PROJECT_ID/$CLOUD_RUN_PROJECT_NAME \
    --project $PROJECT_ID \
    ./$DIRECTORY_PROJECT/
```

### CREAR SERVICIO CLOUD RUN
```
gcloud run deploy $CLOUD_RUN_PROJECT_NAME \
  --image gcr.io/$PROJECT_ID/$CLOUD_RUN_PROJECT_NAME \
  --allow-unauthenticated \
  --project $PROJECT_ID \
  --region $REGION
```

### HACER PUBLICO EL SERVICIO [Opcional]
```
gcloud run services add-iam-policy-binding $CLOUD_RUN_PROJECT_NAME \
  --member="allUsers" \
  --role="roles/run.invoker" \
  --project $PROJECT_ID \
  --region $REGION
```

### OBTENER URL DEL SERVICIO
```
URL_SERVICE=$(gcloud run services describe $CLOUD_RUN_PROJECT_NAME \
  --platform managed \
  --region $REGION \
  --format 'value(status.url)' \
  --project $PROJECT_ID)
```

### REQUEST SERVICE
```
curl ${URL_SERVICE}?user_id=39447924.7392393021
```